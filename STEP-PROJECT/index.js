"use strict";
// Our Services //
const tree = document.querySelectorAll(".tabs-tree");
const tab = document.querySelectorAll(".tab");
const tabsContent = document.querySelectorAll(".tabs-content");

const treeArray = [...tree];
const tabArray = [...tab];
const tabsContentArray = [...tabsContent];

function tabContent() {
  tabArray.forEach((tabs) => {
    tabsContentArray.forEach((contents) => {
      tree.forEach((element) => {
        tabs.classList.remove("tab-color");
        element.classList.add("tabs-hidden");
        contents.classList.add("tabs-hidden-text");
      });
    });
  });
}

tabArray.forEach((tab, tabindex) => {
  tab.addEventListener("click", () => {
    tabsContentArray.forEach((contents, contentsIndex) => {
      treeArray.forEach((tree, elementIndex) => {
        if (tabindex === contentsIndex && tabindex === elementIndex) {
          tabContent();
          tab.classList.add("tab-color");
          tree.classList.remove("tabs-hidden");
          contents.classList.remove("tabs-hidden-text");
        }
      });
    });
  });
});
// our Amazing Work //
const workTabs = document.querySelectorAll(".work-tab");
const workHidden = document.querySelector(".work-hidden");
const workButton = document.querySelectorAll(".work-button");
const workTabsArray = [...workTabs];

const graphicDesign = [
  "./img/graphic design/graphic-design1.jpg",
  "./img/graphic design/graphic-design2.jpg",
  "./img/graphic design/graphic-design3.jpg",
  "./img/graphic design/graphic-design4.jpg",
  "./img/graphic design/graphic-design5.jpg",
  "./img/graphic design/graphic-design6.jpg",
  "./img/graphic design/graphic-design7.jpg",
  "./img/graphic design/graphic-design8.jpg",
  "./img/graphic design/graphic-design9.jpg",
  "./img/graphic design/graphic-design10.jpg",
  "./img/graphic design/graphic-design11.jpg",
  "./img/graphic design/graphic-design12.jpg",
];

const webDesign = [
  "./img/web design/web-design1.jpg",
  "./img/web design/web-design2.jpg",
  "./img/web design/web-design3.jpg",
  "./img/web design/web-design4.jpg",
  "./img/web design/web-design5.jpg",
  "./img/web design/web-design6.jpg",
  "./img/web design/web-design7.jpg",
];

const landingPages = [
  "./img/landing page/landing-page1.jpg",
  "./img/landing page/landing-page2.jpg",
  "./img/landing page/landing-page3.jpg",
  "./img/landing page/landing-page4.jpg",
  "./img/landing page/landing-page5.jpg",
  "./img/landing page/landing-page6.jpg",
  "./img/landing page/landing-page7.jpg",
];

const wordPress = [
  "./img/wordpress/wordpress1.jpg",
  "./img/wordpress/wordpress2.jpg",
  "./img/wordpress/wordpress3.jpg",
  "./img/wordpress/wordpress4.jpg",
  "./img/wordpress/wordpress5.jpg",
  "./img/wordpress/wordpress6.jpg",
  "./img/wordpress/wordpress7.jpg",
  "./img/wordpress/wordpress8.jpg",
  "./img/wordpress/wordpress9.jpg",
  "./img/wordpress/wordpress10.jpg",
];

const firstImage = graphicDesign;
const nextImg = webDesign.concat(landingPages, wordPress);
const allImg = [firstImage, graphicDesign, webDesign, landingPages, wordPress];

function setImages(index) {
  allImg[index].forEach((element) => {
    const tabContainer = document.createElement("div");
    const hover = document.createElement("div");
    const iconContainer = document.createElement("div");
    const icon = document.createElement("img");
    const hoverTitle = document.createElement("a");
    const hoverText = document.createElement("a");
    const img = document.createElement("img");
    workHidden.style.display = "grid";
    tabContainer.classList.add("tab-container");
    hover.classList.add("tab-hover");
    iconContainer.append(icon);
    icon.src = "./img/amazing/icon.png";
    hoverTitle.classList.add("tabTitle");
    hoverText.classList.add("tabText");
    hoverTitle.innerText = "creative design ";
    hoverText.innerText = "Web Design";
    workHidden.append(tabContainer);
    tabContainer.append(hover, img);
    hover.append(iconContainer, hoverTitle, hoverText);
    img.src = element;
  });
}

function getImages() {
  workButton[0].style.display = "flex";
  firstImage.forEach((element, index) => {
    const tabContainer = document.createElement("div");
    const hover = document.createElement("div");
    const iconContainer = document.createElement("div");
    const icon = document.createElement("img");
    const hoverTitle = document.createElement("a");
    const hoverText = document.createElement("a");
    const img = document.createElement("img");
    workHidden.style.display = "grid";
    tabContainer.classList.add("tab-container");
    hover.classList.add("tab-hover");
    img.src = element;
    icon.src = "./img/amazing/icon.png";
    iconContainer.append(icon);
    hoverTitle.classList.add("tabTitle");
    hoverTitle.innerText = " creative design";
    hoverText.classList.add("tabText");
    hoverText.innerText = "Web Design";
    hover.append(iconContainer, hoverTitle, hoverText);
    if (index <= 11) {
      img.src = firstImage[index];
      tabContainer.append(hover, img);
      workHidden.append(tabContainer);
    }
  });
}

getImages();

let imgLabel = 0;

workButton[0].addEventListener("click", (event) => {
  event.target.classList.add("load");

  setTimeout(() => {
    for (let i = 0; i < 12; i++) {
      const tabContainer = document.createElement("div");
      const hover = document.createElement("div");
      const iconContainer = document.createElement("div");
      const icon = document.createElement("img");
      const hoverTitle = document.createElement("a");
      const hoverText = document.createElement("a");
      const img = document.createElement("img");
      tabContainer.classList.add("tab-container");
      hover.classList.add("tab-hover");
      icon.src = "./img/amazing/icon.png";
      iconContainer.append(icon);
      hoverTitle.classList.add("tabTitle");
      hoverTitle.innerText = " ";
      hoverText.classList.add("tabText");
      hoverText.innerText = "Web Design";
      hover.append(iconContainer, hoverTitle, hoverText);
      img.src = nextImg[imgLabel];
      tabContainer.append(hover, img);
      workHidden.append(tabContainer);
      imgLabel++;
      if (imgLabel === 24) {
        imgLabel = 0;
        workButton[0].style.display = "none";
      }
    }
    event.target.classList.remove("load");
  }, 1000);
});

function tabHidden() {
  workTabsArray.forEach((element) => {
    element.classList.remove("work-tab-active");
    workHidden.style.display = "none";
    workHidden.innerText = "";
  });
}
tabHidden();

workTabsArray.forEach((workTab, index) => {
  workTab.addEventListener("click", () => {
    if (index === 0) {
      tabHidden();
      workTab.classList.add("work-tab-active");
      getImages();
    } else if (index === 1 || index === 2 || index === 3 || index === 4) {
      workButton[0].style.display = "none";
      tabHidden();
      workTab.classList.add("work-tab-active");
      setImages(index);
    }
  });
});
// What People SAY//
const clientImageSelected = document.querySelector(".client-chosen");
const clientSelected = document.querySelector(".client-chosen");
const prev = document.querySelector(".prev");
const next = document.querySelector(".next");
const clientName = document.querySelectorAll(".client-name");
const feedback = document.querySelectorAll(".feedback-content");
const peopleArray = [...document.querySelectorAll(".client-wrapper")];

function removeSelected() {
  peopleArray.forEach((e) => {
    e.classList.remove("chosen");
  });
}

function removeStart() {
  clientName.forEach((e) => {
    e.parentElement.classList.remove("start");
  });
}

function getName() {
  clientName.forEach((e) => {
    if (
      e.innerText.toLowerCase() ===
      clientSelected.firstChild.dataset.say.toLowerCase()
    ) {
      removeStart();
      e.parentElement.classList.add("start");
    }
  });
}

function getStory() {
  feedback.forEach((e) => {
    if (
      e.dataset.say.toLowerCase() ===
      clientSelected.firstChild.dataset.say.toLowerCase()
    ) {
      feedback.forEach((e) => {
        e.classList.remove("start");
      });
      e.classList.add("start");
    }
  });
}

let repeat = peopleArray[0].cloneNode(true);
repeat.style.position = "static";
peopleArray[0].classList.add("chosen");
clientSelected.append(repeat);
repeat.classList.add("selected-client");
getName();
getStory();

let selectedIndex = 0;

for (let i = 0; i < peopleArray.length; i++) {
  peopleArray[i].addEventListener("click", () => {
    removeSelected();
    clientImageSelected.innerHTML = "";
    let repeat = peopleArray[i].cloneNode(true);
    repeat.style.position = "static";
    peopleArray[i].classList.add("chosen");
    clientImageSelected.append(repeat);
    repeat.classList.add("selected-client");
    getName();
    getStory();

    selectedIndex = i;
  });
}

prev.addEventListener("click", () => {
  removeSelected();
  clientImageSelected.innerHTML = "";
  selectedIndex--;
  if (selectedIndex < 0) selectedIndex = peopleArray.length - 1;
  let repeat = peopleArray[selectedIndex].cloneNode(true);
  repeat.style.position = "static";
  peopleArray[selectedIndex].classList.add("chosen");
  clientImageSelected.append(repeat);
  repeat.classList.add("selected-client");
  getName();
  getStory();
});

next.addEventListener("click", () => {
  removeSelected();
  clientImageSelected.innerHTML = "";
  selectedIndex++;
  if (selectedIndex === peopleArray.length) selectedIndex = 0;
  let cloneElement = peopleArray[selectedIndex].cloneNode(true);
  cloneElement.style.position = "static";
  peopleArray[selectedIndex].classList.add("chosen");
  clientImageSelected.append(cloneElement);
  cloneElement.classList.add("selected-client");
  getName();
  getStory();
});
//remove reload on page//
const links = document.querySelectorAll("a");
links.forEach(function (link) {
  link.addEventListener("click", function (event) {
    event.preventDefault();
  });
});
//Gallery of best img ??dblclick => wtf
function toggleContent() {
  const content = document.getElementById("gallery-hidden");
  if (content.style.display === "none") {
    content.style.display = "grid";
  } else {
    content.style.display = "none";
  }
}
const loadMore = document.getElementById("loadMore");
loadMore.addEventListener("click", toggleContent);

const searchIcon = document.querySelector(".search-icon");
const input = document.querySelector(".search-input");

searchIcon.addEventListener("click", function () {
  input.style.display = "block";
  input.focus();
});
