const tabs = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelector(".tabs-content");
const tabsContentChild = tabsContent.children;
const tabsArr = [...tabs];
const tabsContentArr = [...tabsContentChild];

function tabsContentRemove(arr) {
  arr.forEach((element) => {
    element.style.display = "none";
  });
}
function removeTabs() {
  tabsContentRemove(tabsContentArr);
  tabsArr.forEach((element) => {
    element.classList.remove("active");
  });
}

removeTabs();

tabsArr.forEach((element, index) => {
  element.addEventListener("click", () => {
    tabsArr.forEach((element) => {
      element.classList.remove("active");
    });
    tabsContentRemove(tabsContentArr);
    element.classList.add("active");
    tabsContentArr[index].style.display = "block";
  });
});
