const img = document.querySelectorAll(".img-to-show");
const timer = document.querySelector(".timer");
const imgArray = [...img];
const start = document.querySelector(".start");
const end = document.querySelector(".end");

let index = 0;
let timeAdding = setInterval(showIMG, 120);

function showIMG() {
  if (index === imgArray.length) {
    index = 0;
    showIMG();
  }

  if (timer.innerText === "0") {
    imgArray[index].style.display = "block";
  }
  if (timer.innerText === "3.0") {
    img[index].classList.add("active");
  }
  if (timer.innerText === "3.00") {
    img[index].classList.remove("active");
    imgArray[index].style.display = "none";
    timer.innerText = 0;
    index++;
    showIMG();
  }
  timer.innerText = (+timer.innerText + 0.1).toFixed(2);
}

end.addEventListener("click", () => {
  clearInterval(timeAdding);
});

start.addEventListener("click", () => {
  clearInterval(timeAdding);
  timeAdding = setInterval(showIMG, 100);
});
