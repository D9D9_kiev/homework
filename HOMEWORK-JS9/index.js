// 1.Опишіть, як можна створити новий HTML тег на сторінці.
//     document.createElement()
// 2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// insertAdjacentHTML(position, text); position определяет позицию добавляемого элемента относительно элемента, вызвавшего метод.
//     beforebegin
// <div>
//     afterbegin
//     beforeend
// </div>
//     afterend
// 3.Як можна видалити елемент зі сторінки?
//     element.remove()
//
const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function arrayToList(arr, parent = document.body) {
  const ul = document.createElement("ul");
  parent.append(ul);
  if (Array.isArray(arr)) {
    arr.forEach((element) => {
      if (Array.isArray(element)) {
        arrayToList(element, ul);
      } else {
        const li = document.createElement("li");
        ul.append(li);
        li.innerText = element;
      }
    });
  }
}

arrayToList(array);
