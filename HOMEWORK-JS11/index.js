const showPassword = document.querySelectorAll(".icon-password");
const inputPassword = document.getElementsByTagName("input");
const button = document.querySelector(".btn");
const showPasswordArray = [...showPassword];
const inputPasswordArray = [...inputPassword];

function eyeSlash(element) {
  element.classList.remove("fa-eye");
  element.classList.add("fa-eye-slash");
}

function eyeSlashRemove(element) {
  element.classList.remove("fa-eye-slash");
  element.classList.add("fa-eye");
}

if (Array.isArray(showPasswordArray)) {
  showPasswordArray.forEach((element, index) => {
    element.addEventListener("click", () => {
      if (element.classList.contains("fa-eye")) {
        eyeSlash(element);
        inputPasswordArray[index].setAttribute("type", "text");
      } else {
        eyeSlashRemove(element);
        inputPasswordArray[index].setAttribute("type", "password");
      }
    });
  });
}

const compare = document.createElement("p");

button.addEventListener("click", (event) => {
  event.preventDefault();
  if (inputPasswordArray[0].value !== inputPasswordArray[1].value) {
    inputPasswordArray[1].parentElement.append(compare);
    compare.style.color = "red";
    compare.innerText = "";
    compare.innerText = "Потрібно ввести однакові значення";
  } else if (
    inputPasswordArray[0].value.length > 0 &&
    inputPasswordArray[1].value.length > 0 &&
    inputPasswordArray[0].value === inputPasswordArray[1].value
  ) {
    compare.remove();
    alert("You are welcome");
  } else {
    compare.remove();
  }
});
