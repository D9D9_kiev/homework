// 1.Чому для роботи з input не рекомендується використовувати клавіатуру?
//     Потому что input запускается каждый раз при смене значения пользователем, и изменение этого значение может быть не только с помощью клавиатуры, оно может быть с помощью мыши,либо же диктовщиком.
//

const button = document.querySelectorAll(".btn");
const buttonArr = [...button];

buttonArr.forEach((element) => {
  document.addEventListener("keydown", (e) => {
    if (e.code === "Key" + element.innerHTML || e.code === element.innerHTML) {
      element.style.background = "blue";
    } else if (element.style.background === "blue") {
      element.style.background = "black";
    }
  });
});
