// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// try...catch используется для отлавливания ошибок (синтаксических,ошибок при выполнении кода), можно отлавливать например ошибки при работе с сервером,при валидации и другие ошибки.
// const numerator= 100, denominator = 'a';
//
// try {
//   console.log(numerator/denominator);
//
//   // забыли объявить переменную a
//   console.log(a);
// }
// catch(error) {
//   console.log('Возникла ошибка');
//   console.log('Сообщение об ошибке: ' + error);
// }

import { books } from "./utils/books.js";

const root = document.getElementById("root");

const bookList = document.createElement("ul");

root.append(bookList);

const elements = ["author", "name", "price"];

books.map((book, index) => {
  const listItem = document.createElement("li");
  try {
    for (const key of elements) {
      if (!book[key]) {
        throw "в object" + index + " не має " + key + " ";
      } else {
        listItem.innerText += book[key] + " ";
      }
    }
    bookList.append(listItem);
  } catch (error) {
    console.error(error);
  }
});
