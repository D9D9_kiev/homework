const geoLocation = "https://api.ipify.org/?format=json";
const info = "http://ip-api.com/json/";

const root = document.querySelector(".root");
const btnModal = document.querySelector(".modal-btn");

btnModal.addEventListener("click", async () => {
  const res = await fetch(geoLocation);
  const { ip } = await res.json();
  const target = await fetch(`${info}${ip}`);
  const data = await target.json();

  const { timezone, country, regionName, city, region } = data;
  const elements = [timezone, country, regionName, city, region];

  elements.forEach((el) => {
    const elInfo = document.createElement("p");
    elInfo.innerText = el;
    root.append(elInfo);
  });
});
