const p = document.querySelectorAll("p");
const links = document.querySelectorAll("a");
const container = document.querySelector("body");
const theme = document.querySelector(".theme");

const textArray = [...p];
const linksArray = [...links];

function darkColor() {
  linksArray.forEach((e) => {
    e.classList.add("theme");
  });
  container.classList.add("theme");
}
textArray.forEach((e) => {
  e.classList.add("theme");
});

function whiteColor() {
  linksArray.forEach((e) => {
    e.classList.remove("theme");
  });
  container.classList.remove("theme");
}
textArray.forEach((e) => {
  e.classList.remove("theme");
});

theme.addEventListener("click", (e) => {
  e.preventDefault();
  if (localStorage.getItem("theme") === "dark") {
    localStorage.removeItem("theme");
  } else {
    localStorage.setItem("theme", "dark");
  }
  setDarkTheme();
});

function setDarkTheme() {
  if (localStorage.getItem("theme") === "dark") {
    darkColor();
  } else {
    whiteColor();
  }
}

setDarkTheme();
