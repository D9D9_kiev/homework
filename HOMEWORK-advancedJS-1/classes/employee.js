export class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  getName() {
    return this.name;
  }
  setName(name) {
    this.name = name;
  }

  getAge() {
    return this.age;
  }
  setAge(age) {
    this.age = age;
  }

  getSalary() {
    return this.salary;
  }
  setSalary(salary) {
    this.salary = salary;
  }
}
