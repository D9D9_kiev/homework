// 1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//
// Смысл прототипного наследования в том, что один объект можно сделать прототипом другого. При этом если свойство не найдено в объекте — оно берётся из прототипа.
//
// 2.Для чого потрібно викликати super() у конструкторі класу-нащадка?
//     для того чтобы был создан обьект к которому мы обратимся через this

import { Programmer } from "./classes/programmer.js";

const programmerOne = new Programmer("Ivan", 22, 1200, "python, JS, SQL");

const programmerTwo = new Programmer(
  "ALex",
  30,
  3200,
  "JAVA, python, HTML, React, SQL"
);

const programmerThree = new Programmer(
  "Petya",
  33,
  5400,
  "AI, superProgrammer"
);

console.log(
  programmerOne,
  programmerOne.getSalary(),
  programmerOne.getAge(),
  programmerOne.getName(),
  programmerOne.getLang()
);
console.log(
  programmerTwo,
  programmerTwo.getSalary(),
  programmerTwo.getAge(),
  programmerTwo.getName(),
  programmerTwo.getLang()
);

console.log(
  programmerThree,
  programmerThree.getSalary(),
  programmerThree.getAge(),
  programmerThree.getName(),
  programmerThree.getLang()
);
