// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
//     AJAX позволяет получать данные с сервера и обновлять содержимое страницы без перезагрузки.
class Movie {
  constructor(film) {
    this.film = film;
  }

  createElement() {
    const filmNumber = document.createElement("div");
    const filmName = document.createElement("h1");
    const description = document.createElement("p");
    const list = document.createElement("ul");

    filmNumber.innerText = `film number: ${this.film.episodeId}`;
    filmName.innerText = this.film.name;
    description.innerText = this.film.openingCrawl;

    this.film.characters.forEach((elem) => {
      const listItem = document.createElement("li");
      listItem.classList.add("loader");
      list.append(listItem);
      this.renderCharacters(elem, listItem);
    });

    return [filmNumber, filmName, description, list];
  }

  renderCharacters(url, text) {
    fetch(url)
      .then((res) => res.json())
      .then((character) => {
        text.innerText = character.name;
        if (text.innerText === character.name) {
          text.classList.remove("loader");
        }
      });
  }

  render() {
    const elements = this.createElement();
    const dataContainer = document.getElementById("filmDataContainer");
    dataContainer.append(...elements);
  }
}

function renderMovies() {
  fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((res) => res.json())
    .then((films) => {
      films.forEach((film) => {
        const movie = new Movie(film);
        movie.render();
      });
    });
}

renderMovies();
